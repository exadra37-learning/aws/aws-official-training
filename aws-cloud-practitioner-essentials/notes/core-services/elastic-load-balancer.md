# ELASTIC LOAD BALANCER

* Also known as the Classic Load Balancer.
* This is a distributed Load Balancer.
* Main features:
    + Sticky Sessions.
        - Binds the user session to the same back-end instance for the duration
            of the session.
    + Health Checks.
    + Cross zone balancing.
    + Multiple Availability Zones.
* Uses cases:
    + Access through single point.
        - Increases security as it will be the only public access point to the
            back-end instances.
    + Decouple application environment.
        - When used as a private load balancer to decouple how internal
            application services talk with each other.
        - When used as a public load balancer to decouple how Internet interact
            with the back-end instances.
    + Provide high availability and fault tolerance.
        - Ability to distribute traffic across multiple Availability Zones.
    + Increase elasticity and scalability.
* Traffic distribution:
    + HTTP/HTTPS requests are distributed using a simple Round Robin algorithm.
    + TCP requests are distributed by using the Least Outstanding Request
        algorithm.
    + Distribution across multiple Availability Zones is enabled by default
        when we create the Load Balancer from a web Interface, but disabled
        when we create it from CLI or an SDK.
* Monitoring Metrics:
    + View HTTP responses.
    + See number of healthy and unhealthy hosts.
    + Filter metrics based on Availability Zones or Load Balancer.
* The Load Balancer will scale itself based on traffic pattern it sees.
* Steps to create this type of load balancer are similar to the ones used to
    create the [Application Load Balancer](aws-application-load-balancer.md#steps-overview-to-create-aload-balancer).


[<<< AWS Application Load Balancer](aws-cloud-practitioner-essentials/notes/core-services/aws-application-load-balancer.md) | [Auto Scaling >>>](aws-cloud-practitioner-essentials/notes/core-services/auto-scaling.md)

[HOME](README.md)
