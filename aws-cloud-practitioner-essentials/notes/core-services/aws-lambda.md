# AWS LAMBDA

* Is a fully managed server-less compute service.
    + No server provisioning, configuration or maintenance needed.
* Event driven execution.
    + CloudWatch triggers.
+ For support:
    + server-less code.
    + micro-services.
* Sub Second Metering:
    + Scales automatically for thousands of requests per second.
* Supports multiple programming languages.
* Pays only for when the code is running.
* Runs in High Availability infrastructures distributed across multiple data centers.
* Includes:
    + Monitoring.
    + Logging.
* Limits:
    + 128KB for the event body that triggers the Lambda function.
    + 6MB of request and body payload.
    + 128MB to 536MB of memory allocation available.
    + 512MB of disk space.
    + 5 minutes of execution time.
    + concurrent executions have a soft limit that can be increase upon request.
    + 100 milliseconds between Lambda functions triggers.
    + package deployment size.
    + number of file descriptors.
* Ideal for intermittent and variable workloads, like:
    + react to changes on:
        - S3 buckets.
        - DinamoDB tables.
    + triggers by http requests.
* Use cases:
    + Automated backups.
    * S3 buckets.
    + Event driven:
        - logging analysis.
        - transformations.
    + IOT(Internet Of Things)
    + Server-less websites.
    + Real Time Stream Processing.
    + For Extract, Transform, Load pipelines of events.
    + Mobile Back-ends.


---

[<<< Amazon Elastic Compute Cloud (EC2)](aws-cloud-practitioner-essentials/notes/core-services/amazon-elastic-compute-cloud.md) | [AWS Elastic Beanstalk >>>](aws-cloud-practitioner-essentials/notes/core-services/aws-elastic-beanstalk.md)

[HOME](README.md)
