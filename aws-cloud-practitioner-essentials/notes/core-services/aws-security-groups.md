# AWS SECURITY GROUPS

* They are like built-in firewalls or virtual firewalls.
* Allow to control how instances on a VPC can be accessed.
* By default:
    + All inbound traffic is blocked.
    + All outbound traffic is allowed.
* A typical example composed by 3 security groups:
    + Web Tier
        + Public.
        + Accepts external traffic on port:
            - 80 from Internet
            - 443 from Internet.
            - 22 from SSH.
    + Application Tier
        + Private.
        + Only accepts internal traffic from the Web Tier.
        + Only accepts external traffic on port:
            - 22 from SSH.
    + Database Tier.
        + Private.
        + Only accepts internal traffic from the Application Tier.
        + Only accepts external traffic on port:
            - 22 from SSH.
* All ports not explicit allowed by the security group configuration are blocked
    to any type of traffic.
* As best practice only open ports that are really needed for the application to run.


---

[<<< Amazon Virtual Private Cloud (VPC)](aws-cloud-practitioner-essentials/notes/core-services/amazon-virtual-private-cloud.md) | [AWS Compute Services >>>](aws-cloud-practitioner-essentials/notes/core-services/aws-compute-services.md)

[HOME](README.md)
