# AWS CORE SERVICES TECHNOLOGY OVERVIEW

On this section the [AWS online course](https://www.aws.training/learningobject/curriculum?id=16357) is composed by 12 videos.




## 1. Welcome

* Alerts that in end we will have a Knowledge check.


## 2. Introduction to Services and Categories

* Services are organized by categories.
* A service can belong to more than one category.
* Full documentation [exists for each service](https://aws.amazon.com/documentation/) without need to be a logged on.








aws-cloud-practitioner-essentials

aws-official-training


