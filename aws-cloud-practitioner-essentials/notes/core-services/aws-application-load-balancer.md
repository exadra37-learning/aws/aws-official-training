# AWS APPLICATION LOAD BALANCER

* Enhanced features to traditional load balancers:
    + Support for more types of request protocols.
        - HTTP.
        - HTTPS.
        - HTTP/2.
        - Web Sockets.
    + CloudWatch metrics.
    + Access logs.
        - Possible to see connection details for web sockets.
    + Health checks.
* Additional features:
    + Path routing.
        - forward requests to different target groups based on the URL path.
    + Host based routing:
        - Supports multiple domains on same load balancer.
        - Based on the domain name the requests can be forwarded to different
            target groups.
    + Native IPv6 support on the VPC.
    + AWS Web Application Firewall(WAF).
    + Dynamic ports.
    + Deletion protection.
    + Request tracing.
* Uses Cases:
    + Routing requests for Containers based on the ports.

* Key Terms:
    + Listeners - Is a process that checks for connection requests, using the
        configured protocol and port. The defined rules for the listener determine
        how the load balancer routes the requests to the targets in one or more
        target groups.
    + Target - Is the destination for the traffic, based on the established
        listener rules.
    + Target Group - Each target group routes requests to one or more registered
        targets using the protocol and port number specified. A target can be registered with multiple target groups. Health checks can be configured
        on a per target group basis.


## Steps Overview to Create a Load Balancer

* Chose between an Application Load Balancer or a Traditional Load Balancer.
* Basic Configuration:
    + Give it the name to be used in the DNS records.
    + Select if is a public or private load balancer.
    + Select IP address type, IPv4 or IPv6.
* Setup Listener protocol and port.
* Select the VPC to be used and assign at least 2 Availability Zones.
* Tag the load balancer for easier identification.
* Configure secure listeners, like SSL for HTTPS.
* Configure the Security Groups.
* Configure Routing Rules:
    + Target Group - the back-end destination
    + Health Checks.
    + Advanced Health Checks.
* Register Targets.
* Review Load Balancer Configuration.
    + Confirm and create it.
* Create other target groups and register them into the load balancer.


[<<< AWS Elastic Beanstalk](aws-cloud-practitioner-essentials/notes/core-services/aws-elastic-beanstalk.md) | [Elastic Load Balancer >>>](aws-cloud-practitioner-essentials/notes/core-services/elastic-load-balancer.md)

[HOME](README.md)
