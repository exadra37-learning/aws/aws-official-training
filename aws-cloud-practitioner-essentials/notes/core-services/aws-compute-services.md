# AWS COMPUTE SERVICES

* Virtual Private Servers:
    + Amazon EC2 instances, that provide a flexible configuration and control.
    + Amazon Lightsail, that gives a complete stack for running for example
        an e-commerce application.
    + Amazon ECS for managed cluster of containers, like Docker ones.
* Serverless computing.
    + Like Lambda functions that run code without the need to manage servers:
        - Pay only for what is used.
        - Zero servers administration.


---

[<<< AWS Security Groups](aws-cloud-practitioner-essentials/notes/core-services/aws-security-groups.md) | [Amazon Elastic Compute Cloud (EC2) >>>](aws-cloud-practitioner-essentials/notes/core-services/amazon-elastic-compute-cloud.md)

[HOME](README.md)
