# AMAZON ELASTIC COMPUTE CLOUD (EC2)

* Referred often as Amazon EC2 instances.
* Pays as you go for only the time an instance is running.
* Broad selection of Hardware and Software to create the EC2 instances.
* Allows for global hosting.


## Steps Overview to Create an EC2 Instance

* Choose a region.
* Launch the EC2 wizard.
* Select the AMI(Amazon Machine Image) for the software we want to use.
* Select the instance type, where we define the hardware capabilities for the instance.
* Configure Instance Details:
    + VPC network.
    + Subnet.
    + Public IP.
    + IAM role.
    + Define Shutdown behavior.
    + Enable protection against terminal termination.
    + Enable CloudWatch Monitoring.
    + Type of tenancy, that by default is set to run EC2 instance in shared hardware.
    + Advanced features.
* Configure the storage.
* Add Tags, for easy identification of the EC2 instance.
* Security Groups by create a custom one or select from a list.
* Review Instance Configuration and Launch.
* Select or Create the SSH key pairs.
* Connect to the EC2 instance over SSH.


---

[<<< AWS Compute Services](aws-cloud-practitioner-essentials/notes/core-services/aws-compute-services.md) | [AWS Lambda >>>](aws-cloud-practitioner-essentials/notes/core-services/aws-lambda.md)

[HOME](README.md)
