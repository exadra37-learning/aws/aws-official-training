# AWS GLOBAL INFRASTRUCTURE


## Regions

* They are geographic areas that host 2 or more Availability Zones.
* They represent entities that are isolated of each other.
* Resources from one region are not automatically replicated into other regions.


## Availability Zones (AZs)

* Collection of data centers within a region.
* So AZs are isolated from each other, because they are a physical and independent infrastructure.
* They are inter-connected by fast Internet links.
* Their network connectivity is independent of each other. So they are not shared.
* They are isolated to protect against failures.
* Recommended to spread the application across multiple AZs.


## Edge Locations (Amazon Cloud Front)

* They host a content delivery network, also known as CDNs.
* Requests are routed to nearest Edge Location from where the user is.
* Thus allows for fast deliver of content to end users.
* They are normally located in high population density zones.


---

[<<< Home](README.md) | [Amazon Virtual Private Cloud (VPC) >>>](aws-cloud-practitioner-essentials/notes/core-services/amazon-virtual-private-cloud.md)

[HOME](README.md)
