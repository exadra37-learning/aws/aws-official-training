# AWS ELASTIC BEANSTALK

* Is a Platform as a Service(PAAS).
* Allows for quick deployment of our applications.
* Reduces the management complexity, but allows control of it if we wish to:
    + Instance type.
    + Database.
    + Adjust auto scaling.
    + Update the application.
    + Access server log files.
    + Enable Https on load balancer.
* Supports multiple programming languages, platforms and containers.
* We only need to provide the application code and Elastic Beanstalk provides:
    + Application service.
    + HTTP service.
    + Operating System.
    + Language Interpreter.
    + Host.
* Elastic Beanstalk provision may take between 5 to 10 minutes depending on the
    chosen stack.


## Steps Overview to Create an Elastic Beanstalk Service

* Application name and description.
* Choose an environment tier.
* Provide a valid domain or use a AWS auto generated one.
* Select programming language platform or use a custom one.
* Provide an Application:
    + Upload application code.
    + Select an existing application version.
    + Or use a sample application.
* Advanced environment customization, like:
    + EC2 Instance tier type.
    + AZs.
    + Load Balancer.
    + Security.
    + Monitoring.
    + Logging.
    + Database.
    + Network.
    + Notifications.
    + Tags.


---

[<<< AWS Lambda](aws-cloud-practitioner-essentials/notes/core-services/aws-lambda.md) | [AWS Application Load Balancer >>>](aws-cloud-practitioner-essentials/notes/core-services/aws-application-load-balancer.md)

[HOME](README.md)
