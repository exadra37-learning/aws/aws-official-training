# AMAZON VIRTUAL PRIVATE CLOUD (VPC)

* Is a private and virtual network inside the AWS Cloud.
    + the concepts used here are the same of on premise networking.
* Full control of the network configuration.
    + Control over what services are isolated or exposed inside a VPC.
* Composed off several layers of security controls.
    + Allows to accept or deny specific internal or external traffic.
* Other AWS services will deploy into the VPC
    + This make them to inherit the built-in network security.


## Features

* Builds upon high availability of AWS Regions and AZs.
    + An Amazon VPC is incorporated within a Region.
    + Possible to have multiple VPCs per account.
* Subnets
    + They are used to divide a VPC.
    + They allow a VPC to span multiple AZs.
    + They can be private or public.
    + Public subnets need to be attached to a Internet Gateway.
* Route Tables
    + They allow to control the traffic going out of the Subnets.
    + By default subnets can talk with each other, but we can customize it.
* Internet Gateway (IGW)
    + Allows a VPC to access the Internet.
* NAT Gateway
    + Allows private subnet resources to access the Internet.
* Network Access Control Lists (NACL)
    + Controls access to subnets.
    + Is stateless.


---

[<<< AWS Global Infrastructure](aws-cloud-practitioner-essentials/notes/core-services/aws-global-infrastructure.md) | [AWS Security Groups >>>](aws-cloud-practitioner-essentials/notes/core-services/aws-security-groups.md)

[HOME](README.md)
