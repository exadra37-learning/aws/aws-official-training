# AWS TRAINING IN CONTRADICTION WITH DOCS

On the video it says HTTP/HTTPS requests are processed by a Round Robin
algorithm while TCP request uses a Least Outstanding Request algorithm, but in
the [official documentation](https://docs.aws.amazon.com/elasticloadbalancing/latest/userguide/how-elastic-load-balancing-works.html#routing-algorithm) says that is
the other way around.


## Tweet Link

https://twitter.com/Exadra37/status/965255539394514945.
