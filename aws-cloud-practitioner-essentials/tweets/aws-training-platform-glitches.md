# AWS TRAINING PLATFORM USER EXPERIENCE GLITCHES

* When we click to launch a course a new tab will open to trigger a pop-up
    where we will be able to watch the videos...
    So is this really necessary?

* Watching videos is not also made easy for us:

    - video player lacks the visual indicator in the progress bar to say where
        we are in the moment.

    - video player lacks the timer to tell us precisely at what minute and
        second we are in the video.

    - video player lacks the slider to allow as to move back forward and
        backwards easily with a small preview pane.

* So when watching videos is extremely hard to go back and forward and found a
    piece of content you want to watch again once we need to keep guessing
    where we were previously or where we want to go.


# CONCLUSION:

The videos course content is awesome but watching them is really a bad user
experience.

Some can say that the course is free, but I can say that the main purpose is to
get us on-board of AWS cloud that is not cheap at all.

Thus a better User Experience when using the training platform is a very legit
expectation of any User wanting to dive on AWS cloud.


# NOTE:

As per this tweet https://twitter.com/Exadra37/status/965183993317556224.
