# AWS OFFICIAL TRAINING

Notes from following along several courses provide in [AWS Official Training](https://www.aws.training) website.

When watching this videos I wrote down this notes with the main purpose of
helping me to memorize what I am learning on the moment.

This is called **Learning in the Hard Way** and is an habit that I acquired
after have read the book [Learn Python the Hard Way](https://learnpythonthehardway.org/) by [Zed A. Shaw](https://twitter.com/lzsthw).


## AWS CLOUD PRACTITIONER ESSENTIALS

Notes from following along this [AWS official course](https://www.aws.training/learningobject/curriculum?id=16357).

### Course Notes

* **Core Services - AWS Technology Overview**
    + [AWS Global Infrastructure](aws-cloud-practitioner-essentials/notes/core-services/aws-global-infrastructure.md)
    + [Amazon Virtual Private Cloud (VPC)](aws-cloud-practitioner-essentials/notes/core-services/amazon-virtual-private-cloud.md)
    + [AWS Security Groups](aws-cloud-practitioner-essentials/notes/core-services/aws-security-groups.md)
    + [Amazon Elastic Compute Cloud (EC2)](aws-cloud-practitioner-essentials/notes/core-services/amazon-elastic-compute-cloud.md)
    + [AWS Lambda](aws-cloud-practitioner-essentials/notes/core-services/aws-lambda.md)
    + [AWS Elastic Beanstalk](aws-cloud-practitioner-essentials/notes/core-services/aws-elastic-beanstalk.md)
    + [AWS Application Load Balancer](aws-cloud-practitioner-essentials/notes/core-services/aws-application-load-balancer.md)
    + [Elastic Load Balancer](aws-cloud-practitioner-essentials/notes/core-services/elastic-load-balancer.md)
    + [Auto Scaling](aws-cloud-practitioner-essentials/notes/core-services/auto-scaling.md)
